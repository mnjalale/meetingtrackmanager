﻿using MeetingTrackManager.Data.Context;
using MeetingTrackManager.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using System;

namespace MeetingTrackManager.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddCors();


            services.AddDbContext<MeetingTrackManagerContext>(options => options.UseSqlServer(Configuration.GetConnectionString(nameof(MeetingTrackManagerContext))));

            // Configure Structuremap IoC
            var container = new Container();

            container.Configure(config =>
            {
                config.AddRegistry(new MeetingTrackManagerRegistry());
                config.Populate(services);
            });

            return container.GetInstance<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                if (!serviceScope.ServiceProvider.GetService<MeetingTrackManagerContext>().AllMigrationsApplied())
                {
                    serviceScope.ServiceProvider.GetService<MeetingTrackManagerContext>().Database.Migrate();
                    serviceScope.ServiceProvider.GetService<MeetingTrackManagerContext>().EnsureSeeded();
                }
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseMvc();
        }
    }
}
