﻿using MeetingTrackManager.Entities;
using MeetingTrackManager.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MeetingTrackManager.Api.Controllers
{
    [Route("api")]
    public class MasterController : Controller
    {
        private readonly ITracksService _trackService;

        public MasterController(ITracksService trackService)
        {
            _trackService = trackService;
        }

        #region Talks

        [Route("talks/get")]
        [HttpGet]
        public IActionResult GetTalks()
        {
            try
            {
                var talks = _trackService.GetTalks();
                return Ok(talks);

            }
            catch (Exception ex)
            {
                return BadRequest($"Errors occurred while getting talks.\n{ex.Message}");
            }
        }

        
        #endregion


    }
}
