﻿using MeetingTrackManager.DTOs;
using MeetingTrackManager.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MeetingTrackManager.Api.Controllers
{
    [Route("api/tracks")]
    public class TracksController : Controller
    {
        private readonly ITracksService _tracksService;

        public TracksController(ITracksService tracksService)
        {
            _tracksService = tracksService;
        }

        [Route("reset")]
        [HttpPost]
        public IActionResult ResetTrackAllocation()
        {
            try
            {
                var response = _tracksService.ResetTalksAllocation();

                if (!response.HasSucceeded)
                {
                    return BadRequest(response.Message);
                }

                return Ok(response.Message);

            }
            catch (Exception ex)
            {
                return BadRequest($"Errors occurred while resetting track allocation.\n{ex.Message}");
            }
        }
                
        [Route("allocateTalks")]
        [HttpPost]
        public IActionResult AllocateTalksToTracks()
        {
            try
            {
                var response = _tracksService.AllocateTalksToTracks();

                if (!response.HasSucceeded)
                {
                    return BadRequest(response.Message);
                }

                var returnObject = (List<TrackDisplayModel>)response.ReturnObject;

                return Ok(returnObject);

            }
            catch (Exception ex)
            {
                return BadRequest($"Errors occurred while allocating talks to tracks.\n{ex.Message}");
            }
        }

        [Route("get")]
        [HttpGet]
        public IActionResult GetTracks()
        {
            try
            {
                var tracks = _tracksService.GetTracks();
                return Ok(tracks);

            }
            catch (Exception ex)
            {
                return BadRequest($"Errors occurred while getting tracks.\n{ex.Message}");
            }
        }
    }
}
