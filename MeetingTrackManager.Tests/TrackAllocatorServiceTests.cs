using MeetingTrackManager.DTOs;
using MeetingTrackManager.Entities;
using MeetingTrackManager.Entities.Extensions;
using MeetingTrackManager.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace MeetingTrackManager.Tests
{
    [TestClass]
    public class TrackAllocatorServiceTests
    {
        [TestMethod]
        public void GetValueTable()
        {
            var items = new List<TalkModel>()
            {
                new TalkModel() {Position = 1, Value=100,Weight=3},
                new TalkModel() {Position = 2, Value=20,Weight=2},
                new TalkModel() {Position = 3, Value=60,Weight=4},
                new TalkModel() {Position = 4, Value=40,Weight=1}
            };

            var talksAllocator = new TalksAllocatorService();
            var weightLimit = 5;

            var valueTable = talksAllocator.GetValueTable(items, weightLimit);

            Assert.AreEqual(0, valueTable[0, 0]);
            Assert.AreEqual(120, valueTable[2, 5]);
            Assert.AreEqual(40, valueTable[4, 1]);
            Assert.AreEqual(140, valueTable[4, 4]);

        }

        [TestMethod]
        public void AddItemsToKnapSack()
        {
            var items = new List<TalkModel>()
            {
                new TalkModel() {Position = 1, Value=100,Weight=3},
                new TalkModel() {Position = 2, Value=20,Weight=2},
                new TalkModel() {Position = 3, Value=60,Weight=4},
                new TalkModel() {Position = 4, Value=40,Weight=1}
            };

            var talksAllocator = new TalksAllocatorService();
            var weightLimit = 5;
            var valueTable = talksAllocator.GetValueTable(items, weightLimit);

            var addedItems = talksAllocator.GetTalksAddedToSession(valueTable, items, weightLimit);

            Assert.AreEqual(2, addedItems.Count);
            Assert.IsTrue(addedItems.Contains(items.Single(c => c.Position == 1)));
            Assert.IsTrue(addedItems.Contains(items.Single(c => c.Position == 4)));
        }

        [TestMethod]
        public void AddTalksToSessions()
        {
            var talks = new List<Talk>()
                {
                    new Talk () {Order = 9,Title="Woah",LengthInMinutes=30},
                    new Talk () {Order = 10,Title="Sit Down and Write",LengthInMinutes=30},
                    new Talk () {Order = 11,Title="Pair Programming vs Noise",LengthInMinutes=45},
                    new Talk () {Order = 12,Title="Rails Magic",LengthInMinutes=60},
                    new Talk () {Order = 13,Title="Ruby on Rails Why We Should Move On?",LengthInMinutes=60},
                    new Talk () {Order = 14,Title="Clojure Ate Scala (on my project)",LengthInMinutes=45},
                    new Talk () {Order = 15,Title="Programming in the Boondocks of Seattle",LengthInMinutes=30},
                    new Talk () {Order = 16,Title="Ruby vs. Clojure for Back-End Development",LengthInMinutes=30},
                    new Talk () {Order = 17,Title="Ruby on Rails Legacy App Maintenance",LengthInMinutes=60},
                    new Talk () {Order = 18,Title="A World Without HackerNews",LengthInMinutes=30},
                    new Talk () {Order = 19,Title="User Interface CSS in Rails Apps",LengthInMinutes=30},
                    new Talk () {Order = 1,Title="Writing Fast Tests Against Enterprise Rails",LengthInMinutes=60},
                    new Talk () {Order = 2,Title="Overdoing it in Python",LengthInMinutes=45},
                    new Talk () {Order = 3,Title="Lua for the Masses",LengthInMinutes=30},
                    new Talk () {Order = 4,Title="Ruby Errors from Mismatched Gem Versions",LengthInMinutes=45},
                    new Talk () {Order = 5,Title="Common Ruby Errors",LengthInMinutes=45},
                    new Talk () {Order = 6,Title="Rails for Python Developers lightning",LengthInMinutes=35},
                    new Talk () {Order = 7,Title="Communicating Over Distance",LengthInMinutes=60},
                    new Talk () {Order = 8,Title="Accounting-Driven Development",LengthInMinutes=45},
                };

            var talksModel = talks.Select(c => c.ToTalkModel()).OrderBy(c => c.Position).ToList();

            var talksAllocator = new TalksAllocatorService();
            var talkLengthLimit = 180; // Morning session

            var valueTable = talksAllocator.GetValueTable(talksModel, talkLengthLimit);

            var addedItems = talksAllocator.GetTalksAddedToSession(valueTable, talksModel, talkLengthLimit);

            Assert.AreEqual(4, addedItems.Count);
            Assert.IsTrue(addedItems.Contains(talksModel.Single(c => c.Position == 1)));
            Assert.IsTrue(addedItems.Contains(talksModel.Single(c => c.Position == 2)));
            Assert.IsTrue(addedItems.Contains(talksModel.Single(c => c.Position == 3)));
            Assert.IsTrue(addedItems.Contains(talksModel.Single(c => c.Position == 4)));

        }

        [TestMethod]
        public void AllocateTalksToTracks()
        {
            var talks = new List<Talk>()
                {
                    new Talk () {Id = 1,Order = 1,Title="Writing Fast Tests Against Enterprise Rails",LengthInMinutes=60},
                    new Talk () {Id = 2,Order = 2,Title="Overdoing it in Python",LengthInMinutes=45},
                    new Talk () {Id = 3,Order = 3,Title="Lua for the Masses",LengthInMinutes=30},
                    new Talk () {Id = 4,Order = 4,Title="Ruby Errors from Mismatched Gem Versions",LengthInMinutes=45},
                    new Talk () {Id = 5,Order = 5,Title="Common Ruby Errors",LengthInMinutes=45},
                    new Talk () {Id = 6,Order = 6,Title="Rails for Python Developers lightning",LengthInMinutes=35},
                    new Talk () {Id = 7,Order = 7,Title="Communicating Over Distance",LengthInMinutes=60},
                    new Talk () {Id = 8,Order = 8,Title="Accounting-Driven Development",LengthInMinutes=45},
                    new Talk () {Id = 9,Order = 9,Title="Woah",LengthInMinutes=30},
                    new Talk () {Id = 10,Order = 10,Title="Sit Down and Write",LengthInMinutes=30},
                    new Talk () {Id = 11,Order = 11,Title="Pair Programming vs Noise",LengthInMinutes=45},
                    new Talk () {Id = 12,Order = 12,Title="Rails Magic",LengthInMinutes=60},
                    new Talk () {Id = 13,Order = 13,Title="Ruby on Rails Why We Should Move On?",LengthInMinutes=60},
                    new Talk () {Id = 14,Order = 14,Title="Clojure Ate Scala (on my project)",LengthInMinutes=45},
                    new Talk () {Id = 15,Order = 15,Title="Programming in the Boondocks of Seattle",LengthInMinutes=30},
                    new Talk () {Id = 16,Order = 16,Title="Ruby vs. Clojure for Back-End Development",LengthInMinutes=30},
                    new Talk () {Id = 17,Order = 17,Title="Ruby on Rails Legacy App Maintenance",LengthInMinutes=60},
                    new Talk () {Id = 18,Order = 18,Title="A World Without HackerNews",LengthInMinutes=30},
                    new Talk () {Id = 19,Order = 19,Title="User Interface CSS in Rails Apps",LengthInMinutes=30}
                };

            talks = talks.OrderBy(c => c.Order).ToList();
            var talksAllocator = new TalksAllocatorService();

            var tracks = talksAllocator.AllocateTalksToTracks(talks);

            Assert.AreEqual(2, tracks.Count);
            //Assert.IsTrue(addedItems.Contains(talksModel.Single(c => c.Position == 1)));
            //Assert.IsTrue(addedItems.Contains(talksModel.Single(c => c.Position == 2)));
            //Assert.IsTrue(addedItems.Contains(talksModel.Single(c => c.Position == 3)));
            //Assert.IsTrue(addedItems.Contains(talksModel.Single(c => c.Position == 4)));

        }
    }
}
