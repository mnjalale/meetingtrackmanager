﻿using MeetingTrackManager.Entities;
using System.Collections.Generic;

namespace MeetingTrackManager.Services.Contracts
{
    public interface ITalksAllocatorService
    {
        List<Track> AllocateTalksToTracks(List<Talk> talks);
    }
}
