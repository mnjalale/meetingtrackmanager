﻿using MeetingTrackManager.DTOs;
using MeetingTrackManager.Entities;
using MeetingTrackManager.Utils;
using System.Collections.Generic;

namespace MeetingTrackManager.Services.Contracts
{
    public interface ITracksService
    {
        OperationResult ResetTalksAllocation();
        OperationResult AllocateTalksToTracks();
        List<TrackDisplayModel> GetTracks();
        List<Talk> GetTalks();
    }
}
