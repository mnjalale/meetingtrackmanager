﻿using MeetingTrackManager.Data.UnitOfWork.Contracts;
using MeetingTrackManager.DTOs;
using MeetingTrackManager.Entities.Extensions;
using MeetingTrackManager.Services.Contracts;
using MeetingTrackManager.Utils;
using System;
using System.Linq;
using MeetingTrackManager.Entities;
using System.Collections.Generic;

namespace MeetingTrackManager.Services
{
    internal class TracksService : ServiceBase, ITracksService
    {
        protected ITalksAllocatorService talksAllocatorService;

        public TracksService(IUnitOfWork unitOfWork, ITalksAllocatorService allocatorService) : base(unitOfWork)
        {
            talksAllocatorService = allocatorService;
        }

        public OperationResult ResetTalksAllocation()
        {
            try
            {
                // Update the talks by removing the session ids
                var talks = uoW.TalkRepository.Get().ToList();
                foreach (var talk in talks)
                {
                    talk.SessionId = null;
                    uoW.TalkRepository.InsertOrUpdate(talk);
                }

                // Delete the tracks
                var tracks = uoW.TrackRepository.Get().ToList();
                foreach (var track in tracks)
                {
                    uoW.TrackRepository.Delete(track);
                }

                Track.order = 1;

                uoW.Commit();

                var result = new OperationResult(true, "Talks have been reset successfully.");
                return result;

            }
            catch (Exception ex)
            {
                return CreateUnsuccessfulResult(ex);
            }
        }

        public OperationResult AllocateTalksToTracks()
        {
            try
            {
                // First clear the current allocation
                var clearOperationResult = ResetTalksAllocation();

                if (!clearOperationResult.HasSucceeded)
                {
                    return clearOperationResult;
                }

                // Allocate the talks
                var talks = uoW.TalkRepository.Get().ToList();
                var tracks = talksAllocatorService.AllocateTalksToTracks(talks);

                // Add Tracks To database
                foreach (var track in tracks)
                {
                    uoW.TrackRepository.InsertOrUpdate(track);
                }

                uoW.Commit();
                
                tracks = uoW.TrackRepository.Get().ToList();
                
                var result = new OperationResult();
                result.ReturnObject = tracks.OrderBy(c=>c.Order).Select(c => c.ToTrackDisplayModel()).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return CreateUnsuccessfulResult(ex);
            }

        }

        public List<TrackDisplayModel> GetTracks()
        {
            try
            {
                var tracks = uoW.TrackRepository.Get().ToList();


                // Temporary fix, lazy loading doesn't seem to work here
                
                foreach (var track in tracks)
                {
                    track.Sessions = uoW.SessionRepository.Get(c => c.TrackId == track.Id).ToList();

                    foreach (var session in track.Sessions)
                    {
                        session.Talks = uoW.TalkRepository.Get(c => c.SessionId == session.Id).ToList();
                    }
                }

                var trackDisplayModels = tracks.Select(c => c.ToTrackDisplayModel()).ToList(); ;
                return trackDisplayModels;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public List<Talk> GetTalks()
        {
            try
            {
                return uoW.TalkRepository.Get().OrderBy(c => c.Order).ToList();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
