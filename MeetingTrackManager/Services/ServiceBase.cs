﻿using MeetingTrackManager.Data.UnitOfWork.Contracts;
using MeetingTrackManager.Utils;
using System;

namespace MeetingTrackManager.Services
{
    public abstract class ServiceBase
    {
        protected IUnitOfWork uoW;

        public ServiceBase(IUnitOfWork unitOfWork)
        {
            uoW = unitOfWork;
        }

        protected OperationResult CreateUnsuccessfulResult(Exception ex)
        {
            var result = new OperationResult(false, ex.Message);
            return result;
        }

        protected OperationResult CreateUnsuccessfulResult(string message)
        {
            var result = new OperationResult(false, message);
            return result;
        }

    }
}
