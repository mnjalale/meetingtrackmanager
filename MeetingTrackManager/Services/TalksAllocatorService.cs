﻿using MeetingTrackManager.DTOs;
using MeetingTrackManager.Entities;
using MeetingTrackManager.Entities.Extensions;
using MeetingTrackManager.Services.Contracts;
using MeetingTrackManager.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MeetingTrackManager.Services
{
    public class TalksAllocatorService : ITalksAllocatorService
    {

        public int[,] GetValueTable(List<TalkModel> talks, int talkLengthLimit)
        {
            try
            {
                var talksCount = talks.Count;
                var totalRows = talksCount + 1;
                var totalColumns = talkLengthLimit + 1;
                var valueTable = new int[totalRows, totalColumns];

                // Fill first row with 0
                for (int i = 0; i < totalColumns; i++)
                {
                    valueTable[0, i] = 0;
                }

                // Fill first column with 0
                for (int i = 0; i < totalRows; i++)
                {
                    valueTable[i, 0] = 0;
                }

                // Fill the other values
                for (int talkPosition = 1; talkPosition < totalRows; talkPosition++)
                {
                    for (int talkLength = 1; talkLength < totalColumns; talkLength++)
                    {
                        var talk = talks[talkPosition - 1]; // .Single(m => m.Position == talkPosition);
                        var talkWeight = talk.Weight;
                        var talkValue = talk.Value;

                        if (talkWeight > talkLength)
                        {
                            valueTable[talkPosition, talkLength] = valueTable[talkPosition - 1, talkLength];
                        }
                        else
                        {
                            valueTable[talkPosition, talkLength] = Math.Max(valueTable[talkPosition - 1, talkLength], talkValue + valueTable[talkPosition - 1, talkLength - talkWeight]);
                        }
                    }
                }

                return valueTable;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        
        public List<TalkModel> GetTalksAddedToSession(int[,] valueTable, List<TalkModel> talks, int talkLengthLimit)
        {
            try
            {
                var talkPosition = talks.Count;
                var talkLength = talkLengthLimit;
                var addedTalks = new List<TalkModel>();

                while (talkPosition > 0 && talkLength > 0)
                {
                    if (valueTable[talkPosition, talkLength] != valueTable[talkPosition - 1, talkLength])
                    {
                        var talk = talks[talkPosition - 1]; //.Single(m => m.Position == talkPosition);

                        addedTalks.Add(talk);

                        talkLength = talkLength - talk.Weight;
                        talkPosition = talkPosition - 1;
                    }
                    else
                    {
                        talkPosition = talkPosition - 1;
                    }
                }

                return addedTalks;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public List<Track> AllocateTalksToTracks(List<Talk> talks)
        {
            try
            {
                var assignedTalks = new List<TalkModel>();
                var talksToAllocate = talks.Select(c => c.ToTalkModel()).ToList();

                var tracks = new List<Track>();

                while (talksToAllocate.Count > 0)
                {
                    var track = new Track();

                    // Allocate morning session
                    var session = track.Sessions.Single(c => c.Name == SessionType.MorningSession);
                    var sessionLength = 180;
                    var valueTable = GetValueTable(talksToAllocate, sessionLength);
                    var allocatedTalks = GetTalksAddedToSession(valueTable, talksToAllocate, sessionLength);

                    session.Talks = talks.Where(c => allocatedTalks.Select(cc => cc.TalkId).Contains(c.Id)).ToList();

                    talksToAllocate.RemoveAll(c => allocatedTalks.Contains(c));

                    // Allocate afternoon session
                    if (talksToAllocate.Count > 0)
                    {
                        session = track.Sessions.Single(c => c.Name == SessionType.AfternoonSession);
                        sessionLength = 240;
                        valueTable = GetValueTable(talksToAllocate, sessionLength);
                        allocatedTalks = GetTalksAddedToSession(valueTable, talksToAllocate, sessionLength);
                        session.Talks = talks.Where(c => allocatedTalks.Select(cc => cc.TalkId).Contains(c.Id)).ToList();

                        talksToAllocate.RemoveAll(c => allocatedTalks.Contains(c));
                    }

                    tracks.Add(track);
                }

                return tracks;
                
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

    }
}
