﻿namespace MeetingTrackManager.DTOs
{
    public class TalkModel
    {
        public int TalkId { get; set; }
        public int Position { get; set; }
        public int Value { get; set; }
        public int Weight { get; set; }
    }
}
