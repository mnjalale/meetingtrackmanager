﻿using MeetingTrackManager.Entities;
using MeetingTrackManager.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MeetingTrackManager.DTOs
{
    public class TrackDisplayModel
    {
        private readonly Track _track;

        public TrackDisplayModel(Track track)
        {
            _track = track;
        }

        public string Name { get { return _track.Name; } }

        public List<TrackDisplayLineModel> Items
        {
            get
            {
                var items = new List<TrackDisplayLineModel>();

                // Start with morning session talks
                var startTime = new DateTime(1, 1, 1, 9, 0, 0);
                var session = _track.Sessions.Single(c => c.Name == SessionType.MorningSession);

                foreach (var talk in session.Talks.OrderBy(c=>c.Order))
                {
                    items.Add(new TrackDisplayLineModel(startTime,talk.Title,talk.LengthInMinutes));
                    startTime = startTime.AddMinutes(talk.LengthInMinutes);
                }

                // Add lunch
                startTime = new DateTime(1, 1, 1, 12, 0, 0);
                items.Add(new TrackDisplayLineModel(startTime, "Lunch"));

                // Add afternoon sessions
                startTime =startTime.AddMinutes(60);
                session = _track.Sessions.Single(c => c.Name == SessionType.AfternoonSession);
                foreach (var talk in session.Talks.OrderBy(c=>c.Order))
                {
                    items.Add(new TrackDisplayLineModel(startTime, talk.Title, talk.LengthInMinutes));
                    startTime = startTime.AddMinutes(talk.LengthInMinutes);
                }
                
                // Add networking session
                items.Add(new TrackDisplayLineModel(startTime, "Networking Event"));

                return items;
            }
        }

    }
}
