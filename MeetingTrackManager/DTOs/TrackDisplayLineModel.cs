﻿using System;

namespace MeetingTrackManager.DTOs
{
    public class TrackDisplayLineModel
    {
        private static int order = 1;
        private readonly DateTime _startTime;
        private readonly string _talkTitle;
        private readonly int _lengthInMinutes;

        public TrackDisplayLineModel(DateTime startTime, string talkTitle, int lengthInMinutes = 0)
        {
            _startTime = startTime;
            _talkTitle = talkTitle;
            _lengthInMinutes = lengthInMinutes;
            Order = order++;
        }

        public int Order { get; set; }
        
        public string Description
        {
            get
            {
                var description = $"{_startTime.ToString("hh:mmtt")} {_talkTitle}";
                description = _lengthInMinutes > 0 ? $"{description} {_lengthInMinutes}min" : description;

                return description;
            }
        }

    }
}
