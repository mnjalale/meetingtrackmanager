﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeetingTrackManager.Entities
{
    public class Session : EntityBase
    {
        public Session()
        {
            Talks = new List<Talk>();
        }

        [ForeignKey("Track")]
        public int TrackId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime StartTime { get; set; }
        
        public DateTime EndTime { get; set; }

        public Track Track { get; set; }
        public virtual List<Talk> Talks { get; set; }
    }
}
