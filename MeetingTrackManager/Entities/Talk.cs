﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeetingTrackManager.Entities
{
    public class Talk : EntityBase
    {
        [ForeignKey("Session")]
        public int? SessionId { get; set; }

        [Required]
        public int Order { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public int LengthInMinutes { get; set; }
                
        public Session Session { get; set; }
    }
}
