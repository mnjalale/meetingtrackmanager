﻿using MeetingTrackManager.DTOs;

namespace MeetingTrackManager.Entities.Extensions
{
    public static class EntityExtensions
    {
        public static TalkModel ToTalkModel(this Talk talk)
        {
            var talkModel = new TalkModel()
            {
                TalkId = talk.Id,
                Position = talk.Order,
                Value = talk.LengthInMinutes,
                Weight = talk.LengthInMinutes
            };

            return talkModel;
        }

        public static TrackDisplayModel ToTrackDisplayModel(this Track track)
        {
            return new TrackDisplayModel(track);
        }

    }
}
