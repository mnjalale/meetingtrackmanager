﻿using MeetingTrackManager.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MeetingTrackManager.Entities
{
    public class Track : EntityBase
    {
        public static int order = 1;

        public Track()
        {
            // Initialize sessions
            var sessions = new List<Session>()
                            {
                                new Session(){  Name = SessionType.MorningSession, StartTime = new DateTime(1,1,1,9,0,0), EndTime = new DateTime(1,1,1,12,0,0)},
                                new Session(){  Name = SessionType.Lunch, StartTime = new DateTime(1,1,1,12,0,0), EndTime = new DateTime(1,1,1,13,0,0)},
                                new Session(){  Name = SessionType.AfternoonSession, StartTime = new DateTime(1,1,1,13,0,0), EndTime = new DateTime(1,1,1,17,0,0)},
                                new Session(){  Name = SessionType.NetworkingEvent, StartTime = new DateTime(1,1,1,17,0,0)},
                            };

            Sessions = sessions;
            Order = order++;
            Name = $"Track {Order}";
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Order { get; set; }

        public virtual List<Session> Sessions { get; set; }

    }
}
