﻿namespace MeetingTrackManager.Utils
{
    public static class SessionType
    {
        public static string MorningSession => "Morning Session";
        public static string AfternoonSession => "Afternoon Session";
        public static string Lunch => "Lunch";
        public static string NetworkingEvent => "Networking Event";
    }
}
