﻿namespace MeetingTrackManager.Utils
{
    public class OperationResult
    {
        public OperationResult()
        {
            HasSucceeded = true;
            Message = "Success";
        }
        public OperationResult(bool hasSucceeded = true, string message = "")
        {
            HasSucceeded = hasSucceeded;
            Message = message;
        }

        public bool HasSucceeded { get; set; }
        public string Message { get; set; }
        /// <summary>
        /// Used to envelope created/updated object
        /// </summary>
        public object ReturnObject { get; set; }
    }
}
