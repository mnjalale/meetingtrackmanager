﻿using MeetingTrackManager.Data.Context;
using MeetingTrackManager.Data.Repositories;
using MeetingTrackManager.Data.Repositories.Contracts;
using MeetingTrackManager.Data.UnitOfWork;
using MeetingTrackManager.Data.UnitOfWork.Contracts;
using MeetingTrackManager.Services;
using MeetingTrackManager.Services.Contracts;
using StructureMap;

namespace MeetingTrackManager.IoC
{
    public class MeetingTrackManagerRegistry : Registry
    {
        public MeetingTrackManagerRegistry()
        {
            // Context
            For<MeetingTrackManagerContext>().Use<MeetingTrackManagerContext>();

            // Unit Of Work
            For<IUnitOfWork>().Use<UnitOfWork>();

            // Repositories
            For<ISessionRepository>().Use<SessionRepository>();
            For<ITalkRepository>().Use<TalkRepository>();
            For<ITrackRepository>().Use<TrackRepository>();

            // Services
            For<ITalksAllocatorService>().Use<TalksAllocatorService>();
            For<ITracksService>().Use<TracksService>();
        }
    }
}
