﻿using MeetingTrackManager.Data.Repositories.Contracts;
using System;

namespace MeetingTrackManager.Data.UnitOfWork.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        ISessionRepository SessionRepository { get; }
        ITalkRepository TalkRepository { get; }
        ITrackRepository TrackRepository { get; }

        void Commit();
    }
}
