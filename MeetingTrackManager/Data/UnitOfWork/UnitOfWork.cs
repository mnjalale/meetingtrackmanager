﻿using MeetingTrackManager.Data.Context;
using MeetingTrackManager.Data.Repositories;
using MeetingTrackManager.Data.Repositories.Contracts;
using MeetingTrackManager.Data.UnitOfWork.Contracts;
using Microsoft.EntityFrameworkCore;
using System;

namespace MeetingTrackManager.Data.UnitOfWork
{
    internal class UnitOfWork : IUnitOfWork
    {
        private readonly MeetingTrackManagerContext _context;

        public UnitOfWork(MeetingTrackManagerContext context)
        {
            _context = context;
        }

        private ISessionRepository _sessionRepository;
        public ISessionRepository SessionRepository
        {
            get { return _sessionRepository ?? (_sessionRepository = new SessionRepository(_context)); }
        }

        private ITalkRepository _talkRepository;
        public ITalkRepository TalkRepository
        {
            get { return _talkRepository ?? (_talkRepository = new TalkRepository(_context)); }
        }

        private ITrackRepository _trackRepository;
        public ITrackRepository TrackRepository
        {
            get { return _trackRepository ?? (_trackRepository = new TrackRepository(_context)); }
        }

        public void Commit()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Errors occurred while saving: " + ex.Message);
            }
        }

        private bool _disposed;
        public void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
