﻿using MeetingTrackManager.Data.Context;
using MeetingTrackManager.Data.Repositories.Contracts;
using MeetingTrackManager.Entities;
using Microsoft.EntityFrameworkCore;

namespace MeetingTrackManager.Data.Repositories
{
    internal class TrackRepository : RepositoryBase<Track>, ITrackRepository
    {
        public TrackRepository(MeetingTrackManagerContext context) : base(context)
        {
            context.Tracks.Include(c => c.Sessions);
            context.Sessions.Include(c => c.Talks);
        }
    }
}
