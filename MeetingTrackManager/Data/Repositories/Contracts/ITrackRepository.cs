﻿using MeetingTrackManager.Entities;

namespace MeetingTrackManager.Data.Repositories.Contracts
{
    public interface ITrackRepository : IRepositoryBase<Track>
    {
    }
}
