﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace MeetingTrackManager.Data.Repositories.Contracts
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "");

        T GetById(object id);

        void Delete(object id);

        void Delete(T entity);

        void InsertOrUpdate(T entity);
    }

}
