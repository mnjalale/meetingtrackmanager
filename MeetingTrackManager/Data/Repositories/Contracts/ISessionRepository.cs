﻿using MeetingTrackManager.Entities;

namespace MeetingTrackManager.Data.Repositories.Contracts
{
    public interface ISessionRepository : IRepositoryBase<Session>
    {
    }
}
