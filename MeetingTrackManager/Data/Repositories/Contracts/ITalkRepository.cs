﻿using MeetingTrackManager.Entities;

namespace MeetingTrackManager.Data.Repositories.Contracts
{
    public interface ITalkRepository : IRepositoryBase<Talk>
    {
    }
}
