﻿using MeetingTrackManager.Data.Context;
using MeetingTrackManager.Data.Repositories.Contracts;
using MeetingTrackManager.Entities;

namespace MeetingTrackManager.Data.Repositories
{
    internal class TalkRepository : RepositoryBase<Talk>, ITalkRepository
    {
        public TalkRepository(MeetingTrackManagerContext context) : base(context)
        {
        }
    }
}
