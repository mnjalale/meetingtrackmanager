﻿using MeetingTrackManager.Data.Context;
using MeetingTrackManager.Data.Repositories.Contracts;
using MeetingTrackManager.Entities;
using Microsoft.EntityFrameworkCore;

namespace MeetingTrackManager.Data.Repositories
{
    internal class SessionRepository : RepositoryBase<Session>, ISessionRepository
    {
        public SessionRepository(MeetingTrackManagerContext context) : base(context)
        {
            context.Sessions.Include(c => c.Talks);
        }
    }
}
