﻿using MeetingTrackManager.Data.Context;
using MeetingTrackManager.Data.Repositories.Contracts;
using MeetingTrackManager.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace MeetingTrackManager.Data.Repositories
{
    internal class RepositoryBase<T> : IRepositoryBase<T> where T : EntityBase
    {
        internal MeetingTrackManagerContext Context;
        internal DbSet<T> DbSet;

        public RepositoryBase(MeetingTrackManagerContext context)
        {
            this.Context = context;
            this.DbSet = context.Set<T>();
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            try
            {
                IQueryable<T> query = DbSet;

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
                
                return orderBy != null ? orderBy(query) : query;
            }
            catch (Exception)
            {

                throw new Exception($"Errors occurred while getting {nameof(T)}!");
            }
        }

        public T GetById(object id)
        {
            try
            {
                return DbSet.Find(id);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting by Id");
            }
        }

        public void Delete(object id)
        {
            try
            {
                var entityToDelete = GetById(id);
                Delete(entityToDelete);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while deleting!");
            }
        }

        public void Delete(T entityToDelete)
        {
            try
            {
                DbSet.Remove(entityToDelete);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while deleting!");
            }
        }

        public void InsertOrUpdate(T entity)
        {
            try
            {
                if (entity.Id <= 0)
                {
                    Insert(entity);
                }
                else
                {
                    Update(entity);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        void Insert(T entity)
        {
            try
            {
                DbSet.Add(entity);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while inserting!");
            }
        }

        void Update(T entityToUpdate)
        {
            try
            {
                DbSet.Attach(entityToUpdate);
                Context.Entry(entityToUpdate).State = EntityState.Modified;
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while updating!");
            }
        }

    }
}
