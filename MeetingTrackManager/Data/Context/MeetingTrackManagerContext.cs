﻿using MeetingTrackManager.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace MeetingTrackManager.Data.Context
{
    public class MeetingTrackManagerContext : DbContext
    {
        public MeetingTrackManagerContext(DbContextOptions<MeetingTrackManagerContext> options)
            : base(options)
        {

        }
               
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Talk> Talks { get; set; }
        public DbSet<Track> Tracks { get; set; }

    }
}
