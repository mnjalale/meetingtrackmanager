﻿using MeetingTrackManager.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MeetingTrackManager.Data.Context
{
    public static class ContextExtensions
    {
        public static bool AllMigrationsApplied(this DbContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                .GetAppliedMigrations()
                .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                .Migrations
                .Select(m => m.Key);

            return !total.Except(applied).Any();
        }

        public static void EnsureSeeded(this MeetingTrackManagerContext context)
        {
            // Seed talks
            if (!context.Talks.Any())
            {
                var talks = new List<Talk>()
                {
                    new Talk () {Order = 1,Title="Writing Fast Tests Against Enterprise Rails",LengthInMinutes=60},
                    new Talk () {Order = 2,Title="Overdoing it in Python",LengthInMinutes=45},
                    new Talk () {Order = 3,Title="Lua for the Masses",LengthInMinutes=30},
                    new Talk () {Order = 4,Title="Ruby Errors from Mismatched Gem Versions",LengthInMinutes=45},
                    new Talk () {Order = 5,Title="Common Ruby Errors",LengthInMinutes=45},
                    new Talk () {Order = 6,Title="Rails for Python Developers lightning",LengthInMinutes=35},
                    new Talk () {Order = 7,Title="Communicating Over Distance",LengthInMinutes=60},
                    new Talk () {Order = 8,Title="Accounting-Driven Development",LengthInMinutes=45},
                    new Talk () {Order = 9,Title="Woah",LengthInMinutes=30},
                    new Talk () {Order = 10,Title="Sit Down and Write",LengthInMinutes=30},
                    new Talk () {Order = 11,Title="Pair Programming vs Noise",LengthInMinutes=45},
                    new Talk () {Order = 12,Title="Rails Magic",LengthInMinutes=60},
                    new Talk () {Order = 13,Title="Ruby on Rails Why We Should Move On?",LengthInMinutes=60},
                    new Talk () {Order = 14,Title="Clojure Ate Scala (on my project)",LengthInMinutes=45},
                    new Talk () {Order = 15,Title="Programming in the Boondocks of Seattle",LengthInMinutes=30},
                    new Talk () {Order = 16,Title="Ruby vs. Clojure for Back-End Development",LengthInMinutes=30},
                    new Talk () {Order = 17,Title="Ruby on Rails Legacy App Maintenance",LengthInMinutes=60},
                    new Talk () {Order = 18,Title="A World Without HackerNews",LengthInMinutes=30},
                    new Talk () {Order = 19,Title="User Interface CSS in Rails Apps",LengthInMinutes=30}
                };

                context.AddRange(talks);
                context.SaveChanges();
            }
            
        }
    }
}
