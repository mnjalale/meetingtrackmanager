﻿(function () {
    'use strict';

    angular.module('app').factory('masterService', ['$http', '$q', 'appSettings', masterService]);

    function masterService($http, $q, appSettings) {
        var baseUrl = appSettings.apiServiceBaseUri;
        var service = {
            getTalks: getTalks
        };

        return service;

        function getTalks() {
            var url = baseUrl + 'api/talks/get',
                deferred = $q.defer();

            $http.get(url)
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function fail(deferred) {
            return function (rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }

    }

})();