﻿(function () {
    'use strict';

    angular.module('app').factory('tracksService', ['$http', '$q', 'appSettings', tracksService]);

    function tracksService($http, $q, appSettings) {
        var baseUrl = appSettings.apiServiceBaseUri;
        var service = {
            get: get,
            allocateTalks: allocateTalks,
            reset:reset
        };

        return service;

        function get() {
            var url = baseUrl + 'api/tracks/get',
                deferred = $q.defer();

            $http.get(url)
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function allocateTalks() {
            var url = baseUrl + 'api/tracks/allocateTalks',
                deferred = $q.defer();
            $http.post(url).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function reset() {
            var url = baseUrl + 'api/tracks/reset',
                deferred = $q.defer();
            $http.post(url).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }


        function fail(deferred) {
            return function (rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }

    }

})();