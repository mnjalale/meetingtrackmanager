﻿(function () {
    'use strict';
    angular.module('app').controller('homeController', ['$scope', '$rootScope', '$q', '$state', '$stateParams', 'masterService','tracksService', 'blockUI', 'DTOptionsBuilder', 'DTColumnDefBuilder', homeController]);

    function homeController($scope, $rootScope, $q, $state, $stateParams, masterService,tracksService, blockUI, DTOptionsBuilder, DTColumnDefBuilder) {
        var talksBlockUi = blockUI.instances.get('talksBlockUi');
        var tracksBlockUi = blockUI.instances.get('tracksBlockUi');
        var vm = this;
        vm.allocateTalks = allocateTalks;
        vm.resetTalks = resetTalks;
        vm.status = $stateParams.status;
        vm.talks = [];
        vm.tracks = [];

        vm.dtOptions = DTOptionsBuilder.newOptions().withOption('order', []);
        vm.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1)
        ];

        activate();

        function activate() {
            getTalks();
            getTracks();
        }

        function getTalks() {
            talksBlockUi.start("Loading...");
            return masterService.getTalks().then(function (talks) {
                
                vm.talks = talks;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                talksBlockUi.stop();
            });
        }

        function getTracks() {
            tracksBlockUi.start("Loading...");
            return tracksService.get().then(function (tracks) {

                vm.tracks = tracks;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                tracksBlockUi.stop();
            });
        }

        function resetTalks() {
            tracksBlockUi.start("Resetting...");
            return tracksService.reset().then(function () {
                vm.tracks = [];
                getTalks();
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                tracksBlockUi.stop();
            });
        }

        function allocateTalks() {
            tracksBlockUi.start("Allocating...");
            return tracksService.allocateTalks().then(function (tracks) {
                vm.tracks = tracks;
                getTalks();
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                tracksBlockUi.stop();
            });
        }


    }
})();