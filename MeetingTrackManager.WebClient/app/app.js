﻿(function () {
    var app = angular.module('app', ['ui.router', 'LocalStorageModule', 'angular-loading-bar', 'datatables',
        'ui.bootstrap', 'ngMessages', 'ui.mask', 'isteven-multi-select', 'blockUI', 'ngToast', 'fcsa-number',
        'ui.bootstrap.datetimepicker', 'angularMoment']);

    app.config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider            
            .state('app', {
                abstract: true,
                url: '/',
                templateUrl: "/app/master/partials/master.html",
                controller: "masterController",
                controllerAs: "vm"
            })
            .state('app.home', {
                url: "home",
                templateUrl: "app/home/partials/home.html",
                controller: "homeController",
                controllerAs: "vm"
            });

        $urlRouterProvider.when('', gotoIndex);
        $urlRouterProvider.when('/', gotoIndex);

        gotoIndex.$inject = ['$state'];

        function gotoIndex($state) {
            $state.go('app.home');
        }
        $urlRouterProvider.otherwise("/");



    });

    app.constant('appSettings', {
        apiServiceBaseUri: 'http://localhost:62631/'
    });

    app.config(['$httpProvider', 'blockUIConfig', 'ngToastProvider', function ($httpProvider, blockUIConfig, ngToastProvider) {
        blockUIConfig.message = 'Please wait...';
        blockUIConfig.autoInjectBodyBlock = false;
        blockUIConfig.delay = 100;
        ngToastProvider.configure({
            animation: 'slide'
        });
    }]);
       
    app.run([
        "$rootScope", "$timeout", "$window", "blockUI", function ($rootScope, $timeout, $window, blockUI) {

            init();

            function init() {
                blockView();
                scrollTop();
            }

            function blockView() {
                $rootScope.$on('$stateChangeStart', blockUI.start);
                $rootScope.$on('$stateChangeSuccess', blockUI.stop);
                $rootScope.$on('$stateChangeError', blockUI.stop);
            }

            function scrollTop() {
                $rootScope.$on('$stateChangeSuccess', function () {
                    $timeout(function () {
                        $window.scrollTo(0, 0);
                    });
                });
            }
        }
    ]);

})();