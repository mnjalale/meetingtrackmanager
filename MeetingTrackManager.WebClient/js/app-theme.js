// Theme Settings here
var theme = {};
theme.color = '#232233';
theme.textColor = '#fff';

/* No need to change anything here. */
$(".brand-primary").css({
    'background-color': theme.color,
    'color': theme.textColor
});
$(".brand-primary-color").css({
    'color': theme.color
});
$('.brand-primary-text').css({
    'color': theme.textColor,
    'background-color': 'transparent !important'
});
$('.brand-primary-btn, .product.active, .list-group-item.active, .list-group-item.active:focus, .list-group-item.active:hover').css({
    'background-color': theme.color,
    'color': theme.textColor,
    'border-bottom': tinycolor(theme.color).darken(10).toHexString() + ' solid 3px'
    }).hover(
    function(){
        $(this).css({
            'background-color': tinycolor(theme.color).darken(5).toHexString(),
            'color': theme.textColor,
            'border-bottom': tinycolor(theme.color).darken(20).toHexString() + ' solid 3px'
        })},
    function(){
        $(this).css({
            'background-color': theme.color,
            'color': theme.textColor,
            'border-bottom': tinycolor(theme.color).darken(10).toHexString() + ' solid 3px'
        });
    });
$('.brand-primary-border').css({
    'border': tinycolor(theme.color).darken(10).toHexString() + ' solid 1px',
    'border-bottom':tinycolor(theme.color).darken(20).toHexString() + 'solid 3px'
});
$('.brand-active').css({
    'background-color': theme.color,
    'color': theme.textColor,
    'border': tinycolor(theme.color).darken(10).toHexString() + ' solid 1px',
    'border-bottom': tinycolor(theme.color).darken(20).toHexString() + 'solid 3px'
});

$(".nav-pills>li.active>a").css({
    'background-color': theme.color,
    'color': theme.textColor
});
$(".nav-pills>li")
    .on('show.bs.tab', function(){
        $(".nav-pills>li").removeClass('active');
        $(".nav-pills li a").css({
            'background-color': 'transparent',
            'color': 'inherit'
        });
        $(this).addClass('active').find('a').css({
            'background-color': theme.color,
            'color': theme.textColor
        });
    });

